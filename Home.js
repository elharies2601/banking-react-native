import React, {Component} from "react";
import {
    AppRegistry,
    StyleSheet,
    ToastAndroid,
    View,
    Image,
    Text,
    Modal,
    ListView,
    TouchableOpacity
} from "react-native";
//  import {  } from "";
import {
    TabNavigator,
    TabBarBottom
} from "react-navigation";
import Pemberitahuan from './Pemberitahuan'
import Profil from './Profil'
import {
    Header,
    Container,
    Fab,
    List,
    ListItem,
    Thumbnail,
    Body,
    Icon,
    Content,
    Card,
    CardItem,
    Form,
    Item,
    Label,
    Input,
    Picker,
    Button,
    Left,
    Right
} from 'native-base'
import fireB from 'firebase'
import FAB from 'react-native-fab'
import index from "./native-base-theme/components";

import emoney from './model/emoney'

const util = require('util');

class Home extends Component {

    constructor() {
        super();
        this.state = {
            activeFab: true,
            rootDataVendor: [],
            saldo: '',
            modalVisible: false,
            idJenisVendor: '',
            namaJenisVendor: '',
            usernameAccount: '',
            pinAccount: '',
            isMoney: false,
            rootDataEmoney: []
        }
    }

    static navigationOptions = {
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('./img/home.png')}
                style={[{tintColor: tintColor}, styles.styleIkon]}
            />
        )
    };

    /*componentWIllMount() {
        /!*this.setState({
            activeFab: !this.state.activeFab
        })*!/
        this.GetVendorList();
        // this.DebugLogin()
        this.isExists()
    }*/

    componentDidMount() {
        this.GetVendorList();
        // this.DebugLogin()
        this.isExists()
    }

    onValueChange(valueSelected) {
        this.setState({
            idJenisVendor: valueSelected
        })
    }

    toggleModal(visibled) {
        this.setState({modalVisible: visibled})
    }

    DebugLogin() {
        const user = fireB.app().auth().currentUser.uid;
        ToastAndroid.show("User: " + user, ToastAndroid.LONG);
        console.log("User: " + user);
        console.log("Nama Vendor: " + this.state.idJenisVendor)
    }

    showModalAccount() {
        return (
            <Modal onRequestClose={() => console.log("Modal Closed")}
                   animationType={"slide"} visible={this.state.modalVisible} transparent={false}>
                <Content>
                    <Form>
                        <Item stackedLabel>
                            <Label>Username</Label>
                            <Input
                                onChangeText={(username) => this.setState({usernameAccount: username})}
                                value={this.state.usernameAccount}
                            />
                        </Item>
                        <Item stackedLabel>
                            <Label>PIN</Label>
                            <Input
                                secureTextEntry={true}
                                onChangeText={(pin) => this.setState({pinAccount: pin})}
                                value={this.state.pinAccount}
                                keyboard-type={'numeric'}
                                maxLength={6}
                            />
                        </Item>
                        <Label style={{left: 10, top: 10}}> Pilih Vendor Bank </Label>
                        <Picker mode='dropdown' selectedValue={this.state.idJenisVendor} style={{left: 7, top: 3}}
                                onValueChange={(itemValue) => this.onValueChange(itemValue)}>
                            {this.state.rootDataVendor.map((item, index) => {
                                return <Picker.Item label={item.value.namaEmoney} value={item.value.idBank} key={index}/>
                            })}
                        </Picker>
                        <Button block success onPress={() => this.GetNameVendor()}>
                            <Text style={{color: '#fff'}}>Tambah</Text>
                        </Button>
                        <Button block light style={{top: 5}} onPress={() => this.toggleModal(false)}>
                            <Text>Batal</Text>
                        </Button>
                    </Form>
                </Content>
            </Modal>
        )
    }

    GetListEmoney() {
        if (this.state.isMoney) {
            const userUID = fireB.auth().currentUser.uid;
            fireB.database().ref('users/' + userUID + '/eMonies').once("value").then((snapshot) => {
                let arrayEmoney = [];
                snapshot.forEach(data => {
                    const emoney = {
                        nameBank: data.val().name,
                        value: {
                            saldoAccount: data.val().balance,
                            _key: data.key
                        }
                    };
                    arrayEmoney.push(emoney)
                });
                this.setState({rootDataEmoney: arrayEmoney})
            }).catch((error) => {
                console.log(error)
            });

            return (
                <Content>
                    {this.state.rootDataEmoney.map((item, index) => {
                        return (
                            <Card key={index}>
                                <CardItem>
                                    <Text>{item.nameBank}</Text>
                                    <Right>
                                        <Text>Rp. {item.value.saldoAccount}</Text>
                                    </Right>
                                </CardItem>
                            </Card>
                        )
                    })}
                </Content>
            )
        } else {
            return (
                <Content>
                    <Text style={{fontWeight: 'bold', fontSize: 24}}>MASIH KOSONG</Text>
                </Content>
            )
        }

    }

    GetNameVendor() {
        return fireB.database().ref('emonies/' + this.state.idJenisVendor).once("value").then(snapshot => {
            this.setState({
                namaJenisVendor: snapshot.val().name
            })
            this.AddEmoneyToAccount()
        }).catch((error) => {
            console.log("Error Get Name Vendor: " + error)
        })
    }

    GetVendorList() {
        return fireB.database().ref('/emonies/').once("value").then((snapshot) => {
            let arrayMoney = [];
            snapshot.forEach(
                res => {
                    const banks = {
                        namaBank: res.val().name,
                        value: {idBank: res.key, namaEmoney: res.val().name}
                    };
                    arrayMoney.push(banks)
                }
            );
            this.setState({
                rootDataVendor: arrayMoney
            })
        }).catch((error) => {
            console.log("Error Database: " + error)
        })
    }

    AddEmoneyToAccount() {
        const userUID = fireB.auth().currentUser.uid;
        const keyEmoney = fireB.database().ref("users/" + userUID).push().key;
        this.setState({activeFab: !this.state.activeFab});

        if (this.state.usernameAccount != null && this.state.pinAccount != null) {
            //this.GetNameVendor();
            console.log("Nama vendor yang terpilih: "+this.state.namaJenisVendor)
            fireB.database().ref("users/" + userUID).child("eMonies").child(keyEmoney).set({
                idBank: this.state.idJenisVendor,
                name: this.state.namaJenisVendor,
                balance: '1500000',
                username: this.state.usernameAccount,
                pin: this.state.pinAccount
            }).then(() => {
                ToastAndroid.show('Berhasil Menambahkan Account', ToastAndroid.LONG);
                this.setState({
                    pinAccount: '',
                    usernameAccount: '',
                    idJenisVendor: ''
                })
            }).catch((error) => {
                console.log("Gagal Menambah karena " + error);
                this.setState({
                    idJenisVendor:'',
                    usernameAccount:'',
                    pinAccount:'',
                })
                ToastAndroid.show("" + error, ToastAndroid.LONG)
            })
        } else {
            ToastAndroid.show("Username atau PIN tidak boleh kosong", ToastAndroid.LONG);
            this.setState({
                pinAccount: '',
                usernameAccount: '',
                idJenisVendor: ''
            })
        }
    }

    isExists() {
        const userUID = fireB.auth().currentUser.uid;
        fireB.database().ref('users/' + userUID + '/eMonies/').once("value").then((snapshot) => {
            console.log("Apakah akun ini ada Emoneynya ? " + snapshot.exists());
            console.log("Number of Emonies: " + snapshot.numChildren());
            this.setState({isMoney: +snapshot.exists()})
        }).catch((error) => {
            console.log(error)
        })
    }

    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Text style={{fontWeight:'bold', color:'#ffffff'}}> HOME </Text>
                    </Left>
                    <Right>
                        <Button transparent onPress={() => this.toggleModal(true)}>
                            <Image
                                source={require('./img/ic_add_white_36dp_2x.png')}
                                style={{height: 30, width: 30}}
                            />
                        </Button>
                    </Right>
                </Header>
                {this.showModalAccount()}
                {this.GetListEmoney()}

            </Container>
        )

    }
}

const styles = StyleSheet.create({
    container: {},
    styleIkon: {
        width: 20,
        height: 20
    },
    styleIkonFb: {
        color: '#1a237e',
        fontSize: 20
    },
    TouchableOpacityStyle: {

        position: 'absolute',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        right: 30,
        bottom: 30,
    },

    FloatingButtonStyle: {

        resizeMode: 'contain',
        width: 50,
        height: 50,
    }
});

const Tabs = TabNavigator({
        Beranda: {
            screen: Home,
        },
        Notifikasi: {
            screen: Pemberitahuan
        },
        Profil: {
            screen: Profil
        },
    },
    {
        tabBarComponent: TabBarBottom,
        tabBarPosition: 'bottom',
        animationEnabled: true,
        swipeEnabled: true,
        tabBarOptions: {
            showIcon: true,
            style: {
                backgroundColor: '#1a237e'
            },
            labelStyle: {
                fontSize: 14,
                fontStyle: 'normal',
            },
            activeTintColor: '#e91e63',
            inactiveTintColor: 'white'
        },
    });

AppRegistry.registerComponent('Home', () => Tabs);
export default Tabs