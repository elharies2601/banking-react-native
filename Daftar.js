import React, {Component} from 'react'
import {
    AppRegistry,
    StyleSheet,
    Text,
    TouchableHighlight,
    TextInput,
    View,
    Image,
    ToastAndroid
} from 'react-native'
import FirebaseApp from 'firebase'
import {StackNavigator} from 'react-navigation'

const util = require('util')

export default class Daftar extends Component {
    constructor(props) {
        super(props)
        this.unsubscriber = null
        this.state = {
            emailUser: '',
            passwordUser: '',
            user: null,
            isAuthenticated: false
        }
    }

    componentDidMount() {
        this.unsubscriber = FirebaseApp.auth().onAuthStateChanged((changedUser) => {
            this.setState({user: changedUser})
            console.log("User sekarang: " +JSON.stringify(changedUser))
            if (this.state.user != null){
                this.setState({isAuthenticated: true})
            }else{
                this.setState({isAuthenticated: false})
            }
        })
    }

    onSignUp() {
        try {
            const {navigate} = this.props.navigation
            // this.setState({
            //     isAuthenticated: true
            // })
            FirebaseApp.auth().createUserWithEmailAndPassword(this.state.emailUser, this.state.passwordUser).then((loggedInUser) => {
                //this.setState({user: loggedInUser})
                this.sendEmailVerification(loggedInUser)
                navigate('Input',{emailP: this.state.emailUser})
                console.log("Terdaftar sebagai " + this.state.emailUser)
                //ToastAndroid.show("Berhasil Mendaftar",ToastAndroid.LONG)


                this.setState({
                    emailUser:'',
                    passwordUser:'',
                    user:null
                })
            }).catch((error) => {
                ToastAndroid.show("Gagal Mendaftar Karena/n"+error, ToastAndroid.LONG)
                console.log('Gagal Mendaftar karena :' + error)
                this.setState({
                    emailUser:'',
                    passwordUser:'',
                    user:null
                })
            })
        } catch (Error) {
            console.error('Error Karena'+Error)
        }
    }

    sendEmailVerification(user){
        user.sendEmailVerification().then(() =>{
            ToastAndroid.show("Silahkan Cek Email Anda untuk Melakukan Verifikasi",ToastAndroid.LONG)
        }).catch((error) => {
            ToastAndroid.show("Erro Terjadi karena "+error)
            console.log("Error Terjadi Karena "+error)
        })
    }

    render() {
        const {navigate} = this.props.navigation
        return (
            <View style={styles.container}>
                <Image
                    style={styles.ikonDaftar}
                    source={require('./img/ic_account_circle_white_48dp_1x.png')}
                />
                <TextInput
                    placeholder={'Masukkan email Anda'}
                    style={styles.inputTelp}
                    onChangeText={(email) => this.setState({emailUser: email})}
                    value={this.state.emailUser}
                    keyboardType={'email-address'}
                />
                <TextInput
                    placeholder={'Masukkan password Baru'}
                    style={styles.inputPassword}
                    onChangeText={(password) => this.setState({passwordUser: password})}
                    value={this.state.passwordUser}
                    secureTextEntry={true}
                    keyboardType={'default'}
                />
                <TouchableHighlight
                    underlayColor={'#1a237e'}
                    style={styles.btnMasuk}
                    // onPress={() => navigate('Input', {})}
                    onPress={() => this.onSignUp()}
                >
                    <Text style={{textAlign: 'center'}}>Masuk</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    underlayColor={'#1a237e'}
                    style={styles.btnBatal}
                    onPress={() => this.props.navigation.goBack()}>
                    <Text style={{textAlign: 'center'}}>Batal</Text>
                </TouchableHighlight>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#1a237e',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ikonDaftar: {
        width: 50,
        height: 50,
        top: -10
    },
    inputTelp: {
        height: 40,
        //position: 'absolute',
        width: 200,
        top: 9,
        backgroundColor: '#fffafa',
        color: '#4b0082',
        borderColor: '#fffafa',
        borderWidth: 2,
        borderRadius: 7
    },
    btnMasuk: {
        backgroundColor: '#97a4c4',
        height: 30,
        width: 100,
        top: 31,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15
    },
    btnBatal: {
        backgroundColor: '#97a4c4',
        height: 30,
        width: 100,
        top: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15
    },
    inputPassword: {
        height: 40,
        //position: 'absolute',
        width: 200,
        top: 18,
        backgroundColor: '#fffafa',
        color: '#4b0082',
        borderColor: '#fffafa',
        borderWidth: 2,
        borderRadius: 7
    }
})