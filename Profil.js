import React, {Component} from "react";
import {
    AppRegistry,
    StyleSheet,
    Text,
    TouchableHighlight,
    View,
    Image,
    ToastAndroid,
    Modal
} from 'react-native'
import {
    TabNavigator,
    TabBarBottom
} from 'react-navigation'

import {
    Form,
    Item,
    Label,
    Input,
    Button,
    Content
} from 'native-base'

import {NavigationActions} from 'react-navigation'

import iniFirebase from 'firebase'

export default class Profil extends Component {

    static navigationOptions = {
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('./img/profil.png')}
                style={[{tintColor: tintColor}, styles.styleIkon]}
            />
        )
    }

    constructor(props) {
        super(props)
        this.state = {
            namaPengguna: '',
            emailPengguna: '',
            alamatPengguna: '',
            noHpPengguna: '',
            modalVisible: false,
            namaBaru: '',
            alamatBaru: '',
            noHpBaru: '',
            isKosong: false
        }
    }

    componentDidMount() {
        this.getDataUser()
    }

    toggleModal(visibled) {
        this.setState({modalVisible: visibled})
    }

    showModalEdit() {
        return (
            <Modal onRequestClose={() => console.log("Modal Closed")}
                   animationType={"slide"} visible={this.state.modalVisible} transparent={false}
            >
                <Content>
                    <Form style={{marginLeft: -16}}>
                        <Item stackedLabel>
                            <Label>Masukkan Nomor Handphone</Label>
                            <Input
                                keyboardType={'numeric'}
                                maxLength={13}
                                placeholder={this.state.noHpPengguna}
                                /*onChangeText={(noHp) => noHp === '' && this.state.noHpBaru === '' ?
                                    this.setState({noHpBaru: this.state.noHpPengguna, noHp}) :
                                    this.setState({noHpBaru: noHp, noHp})}
                                value={this.state.noHpBaru === '' ? this.state.noHpPengguna : this.state.noHpBaru}*/
                                onChangeText={(noHp) => this.setState({noHpBaru: noHp})}
                                value={this.state.noHpBaru}
                            />
                        </Item>
                        <Item stackedLabel>
                            <Label>Masukkan Nama</Label>
                            <Input
                                placeholder={this.state.namaPengguna}
                                /*onChangeText={(nama) => nama === '' && this.state.namaBaru === '' ?
                                    this.setState({namaBaru: this.state.namaPengguna}) :
                                    this.setState({namaBaru: nama, nama})
                                }
                                value={this.state.namaBaru === '' ? this.state.namaPengguna : this.state.namaBaru}*/
                                onChangeText={(nama) => this.setState({namaBaru: nama})}
                                value={this.state.namaBaru}
                            />
                        </Item>
                        <Item stackedLabel>
                            <Label>Masukkan Alamat</Label>
                            <Input
                                placeholder={this.state.alamatPengguna}
                                /*onChangeText={(alamat) => alamat === '' && this.state.alamatBaru === '' ?
                                    this.setState({alamatBaru: this.state.alamatPengguna}) :
                                    this.setState({alamatBaru: alamat, alamat})
                                }
                                value={this.state.alamatBaru === '' ? this.state.alamatPengguna : this.state.alamatBaru}*/
                                onChangeText={(alamat) => this.setState({alamatBaru: alamat})}
                                value={this.state.alamatBaru}
                                multiline={true}
                                numberOfLines={4}
                            />
                        </Item>
                        <Button block success onPress={() => this.editDataProfile()}>
                            <Text style={{color: '#fff'}}>Simpan</Text>
                        </Button>
                        <Button block light style={{top: 5}} onPress={() => this.toggleModal(false)}>
                            <Text>Batal</Text>
                        </Button>

                    </Form>
                </Content>
            </Modal>
        )
    }

    editDataProfile() {
        const userID = iniFirebase.app().auth().currentUser.uid

        if (this.state.namaBaru === '') {
            ToastAndroid.show("Field Nama Tidak Boleh Kosong", ToastAndroid.LONG)
            this.setState({isKosong: true})
        }

        if (this.state.alamatBaru === '') {
            ToastAndroid.show("Field Alamat Baru Tidak Boleh Kosong", ToastAndroid.LONG)
            this.setState({isKosong: true})
        }

        if (this.state.noHpBaru === '') {
            ToastAndroid.show("Field Nomor HP tidak boleh Kosong", ToastAndroid.LONG)
            this.setState({isKosong: true})
        }

        if (!this.state.isKosong) {
            iniFirebase.database().ref("users/" + userID).update({
                'phoneNumber': this.state.noHpBaru,
                'homeAddress': this.state.alamatBaru,
                'name': this.state.namaBaru
            }).then(() => {
                ToastAndroid.show("Berhasil Diubah, silahkan Login Ulang", ToastAndroid.LONG)
                this.toggleModal(false)
                console.log("Berhasil Merubah Profil ke Firebase")
            }).catch((error) => {
                ToastAndroid.show(error, ToastAndroid.LONG)
                this.setState({
                    noHpBaru: '',
                    alamatBaru: '',
                    namaBaru: ''
                })
            })
        }
    }

    getDataUser() {
        const userId = iniFirebase.app().auth().currentUser.uid
        return iniFirebase.database().ref("users/" + userId).once('value').then((snapshot) => {
            console.log("Nama pengguna profil: " + snapshot.val().name)
            console.log("Email pengguna profil: " + snapshot.val().email)
            console.log("Alamat pengguna profil: " + snapshot.val().homeAddress)
            this.setState({
                namaPengguna: snapshot.val().name,
                emailPengguna: snapshot.val().email,
                alamatPengguna: snapshot.val().homeAddress,
                noHpPengguna: snapshot.val().phoneNumber
            })
        }).catch((error) => {
            console.log("Error Get Data User: " + error)
        })
    }

    reset() {
        return this.props.navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({routeName: 'Login'})
            ]
        }))
    }

    SignOut() {
        const {navigate} = this.props.navigation
        const userNya = iniFirebase.app().auth()
        console.log("UID: " + JSON.stringify(userNya.currentUser))
        iniFirebase.auth().signOut().then(() => {
            // ToastAndroid.show("Sign Out Success from "+userNya.currentUser,ToastAndroid.LONG)
            // console.log("UID: "+JSON.stringify(userNya.currentUser.toJSON()))
            //ToastAndroid.show("Sign Out Success from "+iniFirebase.app().auth().currentUser.uid,ToastAndroid.LONG)
            this.reset()
        }).catch((error) => {
            ToastAndroid.show("" + error, ToastAndroid.LONG)
            console.log("Gagal Sign Out Karena: " + error)
        })
    }

    render() {
        return (
            <View>
                {this.showModalEdit()}
                <View style={styles.headerProfil}>
                    <Image
                        style={styles.styleFotoProfil}
                        source={require('./img/535106-user_512x512.png')}
                    />
                    <Text style={styles.styleNamaHeader}> {this.state.namaPengguna}</Text>
                </View>
                <View style={styles.isiProfil}>
                    <View style={styles.isiProfilContainer}>
                        <Text style={{
                            color: 'black',
                            fontSize: 16,
                            fontWeight: 'normal'
                        }}> {this.state.alamatPengguna} </Text>
                    </View>
                    <View style={styles.isiProfilContainer2}>
                        <Text style={{color: 'black', fontSize: 16, fontWeight: 'normal'}}>
                            {this.state.emailPengguna}</Text>
                    </View>
                    <View style={styles.isiProfilContainer}>
                        <Text style={{
                            color: 'black',
                            fontSize: 16,
                            fontWeight: 'normal'
                        }}> {this.state.noHpPengguna} </Text>
                    </View>
                </View>
                <View style={{margin: 10}}>
                    <TouchableHighlight
                        underlayColor={'#97a4c4'}
                        style={styles.btnLogout}
                        onPress={() => this.SignOut()}
                    >
                        <Text style={{color: 'white'}}>Sign Out</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                        style={styles.btnEdit}
                        underlayColor={'#97a4c4'}
                        onPress={() => this.toggleModal(true)}
                    >
                        <Text style={{color: 'white'}}>Edit</Text>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    // container:{
    //     height:300,
    // },
    headerProfil: {
        backgroundColor: '#1a237e',
        height: 300,
        width: window.innerWidth,
        alignItems: 'center',
    },
    isiProfil: {
        marginTop: 0
    },
    styleIkon: {
        width: 20,
        height: 20
    },
    styleFotoProfil: {
        width: 100,
        height: 100,
        top: 90
    },
    styleNamaHeader: {
        top: 100,
        fontSize: 26,
        color: 'white'
    },
    isiProfilContainer: {
        borderColor: '#a9a9a9',
        backgroundColor: '#a9a9a9',
        justifyContent: 'center',
        height: 50
    },
    isiProfilContainer2: {
        borderColor: '#a9a9a9',
        backgroundColor: 'white',
        justifyContent: 'center',
        height: 50
    },
    btnLogout: {
        backgroundColor: '#1a237e',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        height: 30,
        width: 100,

    },
    btnEdit: {
        backgroundColor: '#1a237e',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        width: 100,
        height: 30,
        left: 240,
        bottom: 29
    }
})