import React, {Component} from 'react'
import {
    StyleSheet,
    Text,
    TextInput,
    AppRegistry,
    TouchableHighlight,
    View,
    Image,
    ToastAndroid
} from 'react-native'
import fire from 'firebase'
import {NavigationActions, StackNavigator} from 'react-navigation'

const util = require('util')
export default class InputBiodata extends Component {
    constructor(props) {
        super(props)
        this.state = {
            namaPengguna: '',
            emailPengguna: '',
            alamatPengguna: '',
            noHpPengguna:''
        }
    }
    componentWillMount(){
        const {params} = this.props.navigation.state
        this.setState({
            emailPengguna: params.emailP
        })
    }

    InputToFirebase() {
        const user = fire.auth().currentUser
        console.log("User UID: " + user.uid)
        var databaseFirebase = fire.database()

        databaseFirebase.ref('users/' + user.uid).set({
            'name': this.state.namaPengguna,
            'email': this.state.emailPengguna,
            'homeAddress': this.state.alamatPengguna,
            'phoneNumber':this.state.noHpPengguna
        }).then(() => {
            ToastAndroid.show("Berhasil", ToastAndroid.LONG)
            this.NavigationToHome()
        }).catch((error) => {
            console.error("Error: " + error)
        })
    }

    NavigationToHome(){
        return this.props.navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions:[
                NavigationActions.navigate({routeName: 'Homepage'})
            ]
        }))
    }

    render() {
        const {navigate} = this.props.navigation
        const {params} = this.props.navigation.state

        return (
            <View style={styles.container}>
                <Image
                    style={styles.ikonInput}
                    source={require('./img/ic_account_circle_white_48dp_1x.png')}
                />
                <TextInput
                    placeholder={'Masukkan Nama Lengkap Anda'}
                    style={styles.inputNama}
                    onChangeText={(nama) => this.setState({namaPengguna: nama})}
                    value={this.state.namaPengguna}
                />
                <TextInput
                    placeholder={params.emailP}
                    style={styles.inputEmail}
                    //onChangeText={(email) => this.setState({emailPengguna: params.emailP})}
                    value={params.emailP}
                    editable={false}
                    keyboardType={'email-address'}
                />
                <TextInput
                    placeholder={'Masukkan No HP Anda'}
                    maxLength={13}
                    style={styles.inputNoHp}
                    onChangeText={(noHp) => this.setState({noHpPengguna: noHp})}
                    value={this.state.noHpPengguna}
                    keyboardType={'numeric'}
                />
                <TextInput
                    placeholder={'Masukkan Alamat Anda'}
                    style={styles.inputAlamat}
                    onChangeText={(alamatP) => this.setState({alamatPengguna: alamatP})}
                    value={this.state.alamatPengguna}
                    multiline={true}
                    numberOfLines={4}
                />
                <TouchableHighlight
                    underlayColor={'#1a237e'}
                    style={styles.btnMasuk}
                    onPress={() => this.InputToFirebase()}
                >
                    <Text style={{textAlign: 'center'}}>Submit</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    underlayColor={'#1a237e'}
                    style={styles.btnBatal}
                    onPress={() => this.props.navigation.goBack()}
                >
                    <Text style={{textAlign: 'center'}}>Batal</Text>
                </TouchableHighlight>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#1a237e',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ikonInput: {
        width: 50,
        height: 50,
        top: -10
    },
    inputNama: {
        height: 40,
        //position: 'absolute',
        width: 200,
        top: 9,
        backgroundColor: '#fffafa',
        color: '#4b0082',
        borderColor: '#fffafa',
        borderWidth: 2,
        borderRadius: 7
    },
    inputEmail: {
        height: 40,
        width: 200,
        top: 20,
        backgroundColor: '#fffafa',
        color: '#4b0082',
        borderColor: '#fffafa',
        borderWidth: 2,
        borderRadius: 7
    },
    inputAlamat: {
        height: 100,
        width: 200,
        top: 50,
        backgroundColor: '#fffafa',
        color: '#4b0082',
        borderColor: '#fffafa',
        borderWidth: 2,
        borderRadius: 7
    },
    btnMasuk: {
        backgroundColor: '#97a4c4',
        height: 30,
        width: 100,
        top: 61,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
        left: 55
    },
    btnBatal: {
        backgroundColor: '#97a4c4',
        height: 30,
        width: 100,
        top: 31,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
        right: 55
    },
    inputNoHp:{
        height: 40,
        width: 200,
        top: 31,
        backgroundColor: '#fffafa',
        color: '#4b0082',
        borderColor: '#fffafa',
        borderWidth: 2,
        borderRadius: 7
    }
})