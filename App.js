/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    TextInput,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    ToastAndroid
} from 'react-native';
import {StackNavigator} from 'react-navigation'
import {NavigationActions} from 'react-navigation'
import Daftar from './Daftar';
import InputBiodata from './InputBiodata'
import Home from './Home'
import Pemberitahuan from './Pemberitahuan'

const util = require('util');
import * as firebase from 'firebase'

const firebaseConfig = {
    // apiKey: "AIzaSyAHVNcXTGjjLCeb78Caqgk_SbSDjttXgXg",
    // authDomain: "fir-react-phone.firebaseapp.com",
    // databaseURL: "https://fir-react-phone.firebaseio.com",
    // projectId: "fir-react-phone",
    // storageBucket: "fir-react-phone.appspot.com",
    // messagingSenderId: "335126841378"
    apiKey: 'AIzaSyCo75X68J3s5-RNLeNrkrAGIQJrhh487dE',
    authDomain: 'e-moneyapp.firebaseapp.com',
    databaseURL: 'https://e-moneyapp.firebaseio.com',
    projectId: 'e-moneyapp',
    storageBucket: 'e-moneyapp.appspot.com',
    messagingSenderId: '817349414873'
};
firebase.initializeApp(firebaseConfig);

class App extends Component {
    /* static navigationOptions = {
      title: 'Login'
    } */

    constructor(props) {
        super(props);
        this.unsubscriber = null;
        this.state = {
            emailUser: '',
            passwordUser: '',
            isAuthenticate: false,
            user: null
        }
    }

    NavigationToHome() {
        return this.props.navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({routeName: 'Homepage'})
            ]
        }))
    }

    componentDidMount() {
        this.unsubscriber = firebase.auth().onAuthStateChanged((changedUser) => {
            console.log("User diganti: " + JSON.stringify(changedUser))
            console.log("Status user ?: "+this.state.user)
            if (changedUser != null) {
                this.setState({user: true})
            } else {
                this.setState({user: false})
            }
        })
    }

    componentWillUnmount() {
        if (this.unsubscriber) {
            this.unsubscriber()
        }
    }

    onLogin() {
        firebase.auth().signInWithEmailAndPassword(this.state.emailUser, this.state.passwordUser).then((loggedUser) => {
            if (loggedUser != null){
                this.setState({user:true})
            }else{
                this.setState({user:false})
            }
            console.log('Login sebagai: ' + JSON.stringify(loggedUser.toJSON()))
            if (loggedUser.emailVerified) {
                switch (this.state.user) {
                    case true:
                        ToastAndroid.showWithGravity("Login Berhasil", ToastAndroid.LONG, ToastAndroid.BOTTOM);
                        this.NavigationToHome();
                    case false:
                        this.setState({
                            emailUser: '',
                            passwordUser: '',
                            isAuthenticate: false,
                            user: null
                        })
                }
            }else{
                loggedUser.sendEmailVerification().then(() => {
                    this.setState({
                        emailUser: '',
                        passwordUser: '',
                        isAuthenticate: false,
                        user: null
                    })
                }).catch((error) => {
                    ToastAndroid.show(""+error, ToastAndroid.LONG)
                    console.log("Email Verification Error: "+error, ToastAndroid.LONG)
                })
                ToastAndroid.show("Email belum terverifikasi, silahkan cek email Anda Untuk verifikasi ulang",ToastAndroid.LONG, ToastAndroid.BOTTOM)
            }
        }).catch((error) => {
            ToastAndroid.show("Gagal Login karena " + error, ToastAndroid.LONG);
            console.log("Gagal Login karena " + error)
            this.setState({
                emailUser: '',
                passwordUser: '',
                isAuthenticate: false,
                user: null
            })
        })
    }

    render() {
        const {navigate} = this.props.navigation;

        return (
            <View style={styles.container}>
                <Image
                    style={styles.ikonLogin}
                    source={require('./img/ic_account_circle_white_48dp_1x.png')}
                />
                <TextInput
                    value={this.state.emailUser}
                    placeholder={'Masukkan Email Anda'}
                    style={styles.inputTelp}
                    keyboardType={'email-address'}
                    onChangeText={(email) => this.setState({emailUser: email})}
                />
                <TextInput
                    value={this.state.passwordUser}
                    placeholder={'Masukkan Password Anda'}
                    style={styles.inputPassword}
                    keyboardType={'default'}
                    secureTextEntry={true}
                    onChangeText={(password) => this.setState({passwordUser: password})}
                />
                <TouchableHighlight
                    onPress={() => this.onLogin()}
                    style={styles.btnMasuk}
                    underlayColor={'#1a237e'}>
                    <Text style={{top: 5, textAlign: 'center', justifyContent: 'center'}}>Login</Text>
                </TouchableHighlight>
                <Text style={styles.txtInginDaftar}>
                    Belum Punya akun ? |
                    <Text style={{color: '#000'}}
                          onPress={() => navigate('Daftar',{}) }
                    >
                        Daftar
                    </Text>
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#1a237e',
    },
    /* welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    }, */
    btnMasuk: {
        backgroundColor: '#97a4c4',
        height: 30,
        width: 100,
        top: 20,
        borderRadius: 15
    },
    ikonLogin: {
        width: 50,
        height: 50,
        top: -10,
    },
    inputTelp: {
        width: 200,
        top: 10,
        backgroundColor: '#fffafa',
        color: '#4b0082',
        borderColor: '#fffafa',
        borderWidth: 2,
        borderRadius: 7,
        height: 40
    },
    txtInginDaftar: {
        bottom: 0,
        color: '#ffffe0',
        textAlign: 'center',
        justifyContent: 'center',
        position: 'absolute',
        fontWeight: 'bold',
        fontSize: 16
    },
    inputPassword: {
        width: 200,
        top: 20,
        backgroundColor: '#fffafa',
        color: '#4b0082',
        borderColor: '#fffafa',
        borderWidth: 2,
        borderRadius: 7,
        height: 40,
        marginBottom: 20
    }
});

const myScreen = StackNavigator({
    Login: {screen: App},
    Daftar: {screen: Daftar},
    Input: {screen: InputBiodata},
    Notif: {screen: Pemberitahuan},
    Homepage: {screen: Home}
}, {
    headerMode: 'none'
});

AppRegistry.registerComponent('App', () => myScreen);
export default myScreen