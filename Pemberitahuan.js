import React, { Component } from "react";
import {
    AppRegistry,
    StyleSheet,
    Text,
    TouchableHighlight,
    View,
    Image,
    ListView
} from 'react-native'
import {
    TabNavigator,
    TabBarBottom
} from 'react-navigation'
import {
    Container,
    Content,
    List,
    ListItem,
    Card,
    CardItem,
    Right,
    Left,
    Badge
} from 'native-base'
import f from 'firebase'
export default class Pemberitahuan extends Component{
    static navigationOptions = {
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('./img/lonceng.png')}
                style={[{tintColor: tintColor}, styles.styleIkon]}
            />

        )
    }

    componentDidMount(){
        this.isTransactions()
    }

    componentWillUnmount(){
        this.isTransactions()
    }

    constructor(props){
        super(props)
        this.state={
            isTransactionsData: false,
            dataRootTransactions: []
        }
    }

    GetNotifications(){
        if (this.state.isTransactionsData){
            f.database().ref("dummyData/data/transactions").once("value").then(snapshost => {
                let arrayTransactions = []
                snapshost.forEach(data => {
                    const transactions = {
                        idTransaksi: data.key,
                        jumlahTransaksi: data.val().amount,
                        tanggal: data.val().date,
                        keterangan: data.val().note
                    }
                    arrayTransactions.push(transactions)
                })
                this.setState({dataRootTransactions: arrayTransactions})
                console.log(this.state.dataRootTransactions.length)
            }).catch(error => {
                console.error(error)
            })

            return(
                <Content>
                    {this.state.dataRootTransactions.map((item, index) => {
                        return(
                            <Card key={index}>
                                <CardItem>
                                    <Text>Tanggal Transaksi: {item.tanggal}</Text>
                                </CardItem>
                                <CardItem>
                                    <Text>Jumlah Transaksi: Rp. {item.jumlahTransaksi}</Text>
                                </CardItem>
                                <CardItem>
                                    <Text>Keterangan Transaksi: {item.keterangan}</Text>
                                </CardItem>
                            </Card>
                        )
                    })}
                </Content>
            )
        }else{
            return (
                <Content>
                    <Text style={{fontWeight: 'bold', fontSize: 24}}>MASIH KOSONG</Text>
                </Content>
            )
        }

    }

    isTransactions(){
        return f.database().ref("dummyData/data/transactions").once("value").then(snapshot => {
            this.setState({isTransactionsData: snapshot.exists()})
        }).catch(error => {
            console.error(error)
        })
    }

    render(){
        return(
                <Container>
                    {this.GetNotifications()}
                </Container>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        marginLeft:-16
    },
    styleIkon:{
        width:20,
        height:20
    }
})